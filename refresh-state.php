<?php

	// get login info
	require_once("../config.php"); 

	// Connect to database
  $conn = new mysqli( $h, $u, $p, $d); 
  if ($conn->connect_error){
    die( $conn-> connect_error);
  }

  /* Select  */
  /*  */
  $query = "SELECT pres, temp, ox1, hum FROM log ORDER BY time DESC LIMIT 1";
  $result = $conn->query( $query); 
  if(!$result) die( $conn->error);

  $row = $result->fetch_assoc();
  echo json_encode($row); //Encode into json for "assembly" by client


  $result->close();
  $conn->close();
?>