

/* 
*/
var chartKeeper = {
  data: [
          {"title":"Temperature","subtitle":"\xB0C" ,"ranges":[0,0] ,"measures":[0,50]  ,"markers":[0]},
          {"title":"Oxygen"     ,"subtitle":"%"     ,"ranges":[0,0],"measures":[0,30]  ,"markers":[0]},
          {"title":"Humidity"   ,"subtitle":"%"     ,"ranges":[0]     ,"measures":[0,100] ,"markers":[0]},
          {"title":"Pressure"   ,"subtitle":"mbar"  ,"ranges":[0]     ,"measures":[0,1100] ,"markers":[0]}
          ],

  createChart: function(selector){
    var margin = {top: 20, right: 30, bottom: 20, left: 110}, //Play with the margins to make them work for what we are displaying
      width = $(selector).parent().parent().parent().outerWidth() - 30 - margin.left - margin.right,
      height = 50 - margin.top - margin.bottom;
  
    var chart = d3.bullet()
      .width(width)
      .height(height);
      
    var svg = d3.select(selector).selectAll("svg")
      .data(this.data)
      .enter()
      .append("svg")
        .attr("class", "bullet")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    //Render the data. Note this cannot be chained on the prev
    svg.call(chart);
    
    var title = svg.append("g")
      .style("text-anchor", "end")
      .attr("transform", "translate(-6," + height / 2 + ")");
    
    title.append("text")
      .attr("class", "title")
      .text(function(d) { return d.title; });
    
    title.append("text")
      .attr("class", "subtitle")
      .attr("dy", "1em")
      .text(function(d) { return d.subtitle; });
    return svg;
  },
    
  //Call any time you need to redraw the chart without changing the data, like on a resize
  redrawChart: function(selector){
    var margin = {top: 20, right: 10, bottom: 20, left: 90}, //Play with the margins to make them work for what we are displaying
      width = $(selector).parent().parent().parent().parent().outerWidth() - 71 - margin.left - margin.right,
      height = 50 - margin.top - margin.bottom;
  
    var chart = d3.bullet()
      .width(width)
      .height(height);
      
    var svg = d3.select(selector).selectAll(".bullet")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .call(chart);
  },
    
  //Call when you want to update the data in the chart and redraw it
  updateChart: function(){
    var margin = {top: 20, right: 10, bottom: 20, left: 90}, //Play with the margins to make them work for what we are displaying
      width = $('.graph').parent().parent().parent().parent().outerWidth() - 71 - margin.left - margin.right,
      height = 50 - margin.top - margin.bottom;
  
    var chart = d3.bullet()
      .width(width)
      .height(height);
      
    this.svg.data(this.data)
      .call(chart.duration(1000));
  },
  
  //When resize is finished, redraw the chart
  onResizeFinished: function(){
    this.redrawChart('.graph'); 
  },

  updateTargets: function(ctrl){
    if(ctrl['temp_en'] == 1){
      this.data[0]['markers'][0] = ctrl['temp_tar'];
      this.data[0]['ranges'][0] = ctrl['temp_tar'] - 1;
      this.data[0]['ranges'][1] = ctrl['temp_tar'] + 1;
    }
    else{
      this.data[0]['markers'] = [0];
      this.data[0]['ranges'] = [0];
    }
    if(ctrl['ox_en'] == 1){
      this.data[1]['markers'][0] = ctrl['ox_tar'];
      this.data[1]['ranges'][0] = ctrl['ox_tar'] - 0.5;
      this.data[1]['ranges'][1] = ctrl['ox_tar'] + 0.5;
    }
    else{
      this.data[1]['markers'] = [0];
      this.data[1]['ranges'] = [0];
    } 
    this.updateChart();
  },

  /* Updates the bullet chart with the information
   * data: JSON string containing an associative array of state values
  */
  updateData: function(inputData){
    var datAry = jQuery.parseJSON(inputData);
    this.data[0]['measures'][0] = parseFloat(datAry['temp']);
    this.data[1]['measures'][0] = parseFloat(datAry['ox1']);
    this.data[2]['measures'][0] = parseFloat(datAry['hum']);
    this.data[3]['measures'][0] = parseFloat(datAry['pres']);

    //TODO: Refresh subtitles ... how?
    this.data[0]['subtitle'] = datAry['temp'] + " \xB0C";
    this.data[1]['subtitle'] = datAry['ox1']  + " %";
    this.data[2]['subtitle'] = datAry['hum']  + " %";
    this.data[3]['subtitle'] = datAry['pres'] + " mbar";
    this.updateChart();
    $('#temp').html(this.data[0]['measures'][0]);
    $('#ox').html(this.data[1]['measures'][0]);
    $('#hum').html(this.data[2]['measures'][0]);    
    $('#pres').html(this.data[3]['measures'][0]);
  }, 
};