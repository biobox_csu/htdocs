<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- External Stylesheets -->
    <link rel="stylesheet" href="./lib/bootstrap-3.3.7/css/bootstrap.min.css">    
    <link rel="stylesheet" href="./lib/select2-4.0.3/css/select2.min.css">
    <link rel="stylesheet" href="./lib/bootstrap-slider/css/bootstrap-slider.min.css">

    <!-- External Libraries -->
    <script src="./lib/d3/d3.v3.min.js"></script>
    <script src="./lib/jquery/1.12.4/jquery.min.js"></script>
    <script src="./lib/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script src="./lib/select2-4.0.3/js/select2.min.js"></script>
    <script src="./lib/bootstrap-slider/bootstrap-slider.min.js"></script>



    <!-- My libraries -->    
    <script src="./require/bullet.js"></script> 
    <script src="./require/chart.js"></script>

    <!-- My Stylesheets -->
    <link rel="stylesheet" href="./css/biobox.css">

    <style>
      h1{ text-align: center; }
      h2{ text-align: center; }
      h3{ text-align: center; }
      h4{ text-align: center; }
      h5{ text-align: center; }
      h6{ text-align: center; }
      .set_field{
        width:62px;
      }
      .centeredCell{
        text-align: center;
        vertical-align: middle;
      }
      
      /*    For graphs  */
      .bullet { font: 10px sans-serif; }
      .bullet .marker { stroke: #000; stroke-width: 0px; }  //markers are invisible, only range for tolerances
      .bullet .tick line { stroke: #666; stroke-width: .5px; }
      .bullet .range.s0 { fill: #00fa9a; }
      .bullet .range.s1 { fill: #fff; }
      .bullet .range.s2 { fill: #fff; }
      .bullet .measure.s0 { fill: lightsteelblue;}
      .bullet .measure.s1 { fill: #204787; }
      .bullet .title { font-size: 14px; font-weight: bold; }
      .bullet .subtitle { fill: #999; }
 
      .graph {width: 90%;}
      td.space {
        padding: 5px 5px 5px 5px;;
      }
    </style>
  </head>

  <body>  
    <div class="container-fluid">

<!-- ROW 1 -->
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-success">
            <h1><strong>Bio Box</strong></h1>
          </div>
        </div>
      </div> 
<!--      <div class="row">
        <div class="col-sm-12">
          <div class="alert alert-info">
            <h2>Configuration</h2>
          </div>
        </div>
      </div> -->

      <!-- Chamber -->
      <div class="row">
        <div class="col-md-4">
          <div class="panel-group">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">
                  <a data-toggle="collapse" href="#EnvironmentCollapse">Environment</a>
                  <!--<div class="graph"></div>-->
                </h3>
              </div>
              <div id="EnvironmentCollapse" class="panel-collapse collapse">
                <div class="panel-body">
                  
                  <!-- CONTROL -->
                  <!--<div class="well">-->
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Parameter</th>
                          <th style="text-align: center">Control</th>
                          <th style="text-align: center">Value</th>
                        </tr>
                      </thead>
                      <tbody>
                      	<tr>
                          <td>Oxygen</td>
                          <td class="centeredCell"><input class="env-ctrl" type="checkbox" value="" id="ox_en"></td>
                          <td class="centeredCell"><input class="form-control set_field env-ctrl" type="text" value="5" id="ox_tar"></td>
                        </tr> 
                        <tr>
                          <td>Temperature</td>
                          <td class="centeredCell"><input class="env-ctrl" type="checkbox" value="" id="temp_en"></td>
                          <td class="centeredCell"><input class="form-control set_field env-ctrl" type="text" value="37.5" id="temp_tar"></td>
                        </tr>                                         
                      </tbody>
                    </table>
                    <!-- Test region for getting update -->
                    <div id='state'>
                      Temp: <span id='temp'>UNINITIALIZED      </span><br>
                      Humidity: <span id='hum'>UNINITIALIZED   </span><br>
                      Oxygen: <span id='ox'>UNINITIALIZED      </span><br>
                      Pressure: <span id='pres'>UNINITIALIZED  </span><br>
                    </div>
                    <!--  -->
                    <div class="graph"></div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Observation -->
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                <a data-toggle="collapse" href="#ObservationCollapse">Observation</a>
              </h3>
            </div>  
            <div id="ObservationCollapse" class="panel-collapse collapse" position="relative">
            <!-- Control move speed in X/Y/Z direction -->
              <form id="rate-input">
                  <!-- Y axis controls -->
                  <table style="margin: 0 auto; ">
                    <tr>
                    <td class="centeredCell">Y</td>
                    <td></td>
                    <td class="centeredCell">Z</td>
                    <tr>
                    <tr>
                      <div name="platform control">
                        <td class="space" >
                          <div class="btn-group-vertical" data-toggle="buttons">                
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="Y" value="-3" id="y0"        > <<< </label>              
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="Y" value="-2" id="y1"        > <<  </label>
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="Y" value="-1" id="y2"        > <   </label>
                            <label class="btn btn-primary active"><input type="radio" autocomplete="off" name="Y" value="0"  id="y3" checked> .   </label>
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="Y" value="1"  id="y4"        > >   </label>
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="Y" value="2"  id="y5"        > >>  </label>
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="Y" value="3"  id="y6"        > >>> </label>
                          </div>
                        </td>
                        
                        <!-- Camera Feed -->
                        <td class="space" >
                          <div> 
                            <!-- Will display dummy image if stream fails to load. For testing without stream, comment this and uncomment below. -->
                            <img class="img-rounded" style="-webkit-user-select: none" src="http://172.24.1.1:8081/" onerror="this.onerror=null; this.src='/dummy-stream.gif'"> 
                            <!-- <img class="img-rounded" style="-webkit-user-select: none" src='/dummy-stream.gif'>  -->
                          </div>
                        </td>
                        <!-- Z axis controls -->
                        <td class="space" style="border-left: solid #AAA;">
                          <div class="btn-group-vertical" data-toggle="buttons">                
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="Z" value="-3" id="z0"        > <<< </label>              
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="Z" value="-2" id="z1"        > <<  </label>
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="Z" value="-1" id="z2"        > <   </label>
                            <label class="btn btn-primary active"  ><input type="radio" autocomplete="off" name="Z" value="0"  id="z3" checked> .   </label>
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="Z" value="1"  id="z4"        > >   </label>
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="Z" value="2"  id="z5"        > >>  </label>
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="Z" value="3"  id="z6"        > >>> </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <!-- X axis controls -->
                        <td style="text-align: right;">X</td>
                        <td class="centeredCell space">
                          <div class="btn-group" data-toggle="buttons">                
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="X" value="-3" id="x0"        > <<< </label>              
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="X" value="-2" id="x1"        > <<  </label>
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="X" value="-1" id="x2"        > <   </label>
                            <label class="btn btn-primary active"><input type="radio" autocomplete="off" name="X" value="0"  id="x3" checked> .   </label>
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="X" value="1"  id="x4"        > >   </label>
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="X" value="2"  id="x5"        > >>  </label>
                            <label class="btn btn-primary"><input type="radio" autocomplete="off" name="X" value="3"  id="x6"        > >>> </label>
                          </div>  
                        </td>
                        <td></td>
                      </tr>
                    </div> 
                  </table> 
              </form>  
            </div>  
          </div>              
        </div>

        <!-- Camera -->
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                <a data-toggle="collapse" href="#CameraCollapse">Camera</a>
              </h3>
            </div>
              <div id="CameraCollapse" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="well">Focus</div>
                  <div class="well">Light</div>
                  <div class="well">Video</div>
                  <div class="well">Photo</div>
                </div>
              </div>
            </div>
          </div>                 
        </div>
      </div>
      </div>

<!-- ROW 2 -->
        <!-- Experiments -->
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                <a data-toggle="collapse" href="#DataCollapse">Experiments</a>
              </h3>
            </div>
            <div id="DataCollapse" class="panel-collapse collapse">
              <div class="panel-body">

                <div class="well">
                    <select class="experiment-selector" style="width: 100%">
                      <option selected="selected">Select Experiment</option>
                    </select>
                </div>

                <div class="well">
                    <h4>Recording</h4>
                    <div align="center">
                        <a href="#" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-play"></span></a>
                        <a href="#" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-pause"></span></a>
                        <a href="#" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-stop"></span></a>
                    </div>
                </div>

                <!-- HISTORY -->
                <div class="well">
                    <h4>Transfer Records</h4>
                    <div>     
                        <select class="experiment-selector" style="width: 100%">
                          <option selected="selected">Select Experiment</option>
                        </select>
                        <select class="record-selector" style="width: 100%">
                              <option selected="selected">Select Record</option>
                        </select>  
                    </div>  
                    <div>
                        <button type="button" class="btn btn-default btn-lg btn-block"><span class="alignleft">Logs  </span><span class="alignright">.zip&nbsp<span class="glyphicon glyphicon-save"></span></span></button>
                        <button type="button" class="btn btn-default btn-lg btn-block"><span class="alignleft">Images</span><span class="alignright">.zip&nbsp<span class="glyphicon glyphicon-save"></span></span></button>
                        <button type="button" class="btn btn-default btn-lg btn-block"><span class="alignleft">Video </span><span class="alignright">.zip&nbsp<span class="glyphicon glyphicon-save"></span></span></button>
                        <button type="button" class="btn btn-default btn-lg btn-block"><span class="alignleft">All   </span><span class="alignright">.zip&nbsp<span class="glyphicon glyphicon-save"></span></span></button>
                    </div>                          
                    
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <!-- Calibration -->
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                <a data-toggle="collapse" href="#CalCollapse">Calibration</a>
              </h3>
            </div>
            <div id="CalCollapse" class="panel-collapse collapse">
              <div class="panel-body">
                <div class="well">
                 <h5><button type="button" class="btn btn-default">Calibrate Gas Control</button></h5>
                  <div class="progress">
                    <div class="progress">
                      <div class="progress-bar" role="progressbar" aria-valuenow="70"
                      aria-valuemin="0" aria-valuemax="100" style="width:70%">
                        70%
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  <script type="text/javascript">
  function formatRepo (repo) {
    if (repo.loading) return repo.text;

    var markup = "<div class='select2-result-repository clearfix'>" +
    "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
    "<div class='select2-result-repository__meta'>" +
    "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

    if (repo.description) {
      markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
    }

    markup += "<div class='select2-result-repository__statistics'>" +
    "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
    "<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
    "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
    "</div>" +
    "</div></div>";

    return markup;
  }

  function formatRepoSelection (repo) {
    return repo.full_name || repo.text;
  }

  var pause = false;
  //Runs only after all HTML (DOM) is ready
  $(document).ready(function(){ 
    // Create the chart
    chartKeeper.svg = chartKeeper.createChart(".graph");

    /*  Event: environmental control settings have been modified
     *  Response: 
     *    update database with new settings
     *    update chart with new target (chartKeeper handles tolerances)
     *    Note: target == 0 for non-controlled, must pass enable status with targets
    */
    $(".env-ctrl").change(function(){
      pause = false;
      // update database
      var state = {
        temp_tar: parseFloat($("#temp_tar").val()),
        temp_en: $("#temp_en").is(":checked") == true ? 1 : 0,
        ox_tar: parseFloat($("#ox_tar").val()),
        ox_en: $("#ox_en").is(":checked") == true ? 1 : 0
      }
      $.post("./update-control.php", state);
      // update chart with new targets
      chartKeeper.updateTargets.bind(chartKeeper)(state);
      pause = false;
    });

    /*  Event: movement rate change
        Response: update database with new rates
    */
    $("#rate-input input").change(function(){
      // update database
      rates = {
        xRate: $('input[name=X]:checked', '#rate-input').val(),
        yRate: $('input[name=Y]:checked', '#rate-input').val(),
        zRate: $('input[name=Z]:checked', '#rate-input').val()
      }
      // alert(rates['x'] + ', ' + rates['y'] + ', ' + rates['z'] );
      $.post("./update-rates.php", rates);
    });

    /*  Event: sliders have moved
     *  Response: 
     *    update database with new value for sliders
    */

        
    /*  Event: window has been resized
     *  Response:
     *    wait for window to finish resizing
     *    Adjust graph 
    */	
    $(window).resize(function(){
        if(window.waitResizeFinished) clearTimeout(window.waitResizeFinished);
        window.waitResizeFinished = setTimeout(chartKeeper.onResizeFinished.bind(chartKeeper)(".graph"), 100);
    });    
    		
    /*  Event: 1 second has elapsed
     *  Response:
     *    retrieve most recent values from database
     *    update bullets to reflect new values
    */
    setInterval(function(){
      if(!pause){
        $.post('refresh-state.php', null, chartKeeper.updateData.bind(chartKeeper));
      }      
      }, 1000);
      

      /************************ SELECTOR FOR EXPERIMENT *************************/
      $(".experiment-selector").select2({
        ajax: {
          type: 'POST',
          url: "get_experiment-structure.php",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
          },
          processResults: function (data, params) {
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            params.page = params.page || 1;

            return {
              results: data.items,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: true
        },
        minimumResultsForSearch: -1,
        minimumInputLength: -1
      });

      /**************************** SELECTOR FOR LOG ****************************/
      $(".record-selector").select2({
        ajax: {
          type: 'POST',
          url: "./php/experiments/get_experiment-structure.php",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
          },
          processResults: function (data, params) {
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            params.page = params.page || 1;

            return {
              results: data.items,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumResultsForSearch: -1
      });

  });
  </script>

  </body>
</html>

