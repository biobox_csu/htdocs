import MySQLdb
import config
from time import sleep
import sys
import math
from SystemTemp import SystemTemp
from SystemHum import SystemHum 
from SystemPres import SystemPres
from SystemOx import SystemOx
import argparse

def executeQuery(query, args):
	try:
                cnx = MySQLdb.connect(host=config.hostname, user=config.username, passwd=config.password, db=config.database)
                cursor = cnx.cursor()
                cursor.execute(query, args)
                cnx.commit()
	except MySQLdb.Error as e:
		try:
                        print "MySQLdb Error [{}]: {}".format(e.args[0], e.args[1])
                except:
                        print "MySQL Error: {}".format(e)
	else:
	  cnx.close()

def addToLog(vals):
	query = "INSERT INTO log (pres, temp, ox1, hum) VALUES (%s, %s, %s, %s)"
	args = (vals['P'], vals['T'], vals['O'], vals['H'])  
	executeQuery(query, args)

def truncateTable(table):
	query = "TRUNCATE TABLE " + table
	args = None
	executeQuery(query, args)


def runExperiment(ticks):
	systemTemp = SystemTemp(37.5)
	systemHum = SystemHum(80)
	systemPres = SystemPres()
	systemOx = SystemOx(5)

	vals = {'T': 23, 'P': 1015, 'O': 21, 'H': 45}

	for i in range(ticks):
		vals['T'] 	= round(systemTemp.tickMaintain() - 273.15, 3)
		vals['P'] 	= round(systemPres.tickMaintain(), 3)
		vals['O'] 	= round(systemOx.tickMaintain(), 3)
		vals['H'] 	= round(systemHum.tickMaintain(), 3)
		printVals(vals)
		addToLog(vals)
		sleep(1)

def printVals(vals):
        valRes = ""
	for key in vals:
		valRes += "{:<3} {:<8} ".format(key+":", vals[key])
	print(valRes)

def main():
	parser = argparse.ArgumentParser(description="Generate sample data for biobox")
	parser.add_argument('-t', '--ticks', metavar='t', type=int, help = "number of 1 second ticks the simulation should generate", required=True)
	parser.add_argument('-n', '--new', action='store_true', help = "set to overwrite rather than append the table")
	args = parser.parse_args()

	if(args.new):
                truncateTable('log')
	runExperiment(args.ticks)	

if __name__ == '__main__':
	main()
