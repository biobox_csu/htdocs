import random
import math

class SystemHum():
  def __init__(self, target):
    self.current = random.uniform(40,50)
    self.lower = target*.95
    self.upper = target*1.05
    self.hum_on = False;

  def tickUp(self):
    self.current = .95*self.current + 5

  def tickDown(self):
    self.current = .95*self.current

  def tickMaintain(self):
    if self.hum_on == True:
      if self.current < self.upper:
        self.tickUp()
      else:
        self.hum_on = False
        self.tickDown()
    else:
      if self.current < self.upper and self.current > self.lower:
        self.tickDown()
      else:
        self.hum_on = True
        self.tickUp()
    return self.current