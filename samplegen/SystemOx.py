from time import sleep
import math


class SystemOx():

  def __init__(self, targetPercent):
    self.ambientO2 = .21
    self.currentRatio = .21
    self.vol = 29
    self.target = targetPercent/100
    self.lower = self.target - .005
    self.upper = self.target + .005
    self.airDisp = 1.59/self.vol  # (L/sec)/vol = ratio of chamber air displaced per tick
    self.cdDisp = 2.58/self.vol   # (L/sec)/vol = ratio of chamber air displaced per tick
    self.idleDisp = .001          # arbitrary small ratio for idle
    self.reduce = False
    self.target = targetPercent/100

  def printSettings(self):
    print("LowRatio: {}\n HighRatio: {}\n".format(self.lower, self.upper))

  def tickAir(self):
    return self.currentRatio*(1-self.airDisp) + self.airDisp*self.ambientO2

  def tickCD(self):
    return self.currentRatio*(1-self.airDisp)

  def tickNone(self):
    return self.currentRatio*(1-self.airDisp) + self.idleDisp*self.ambientO2

  def tickMaintain(self):
    if self.reduce == True:               # ratio too low, increase
      if self.currentRatio > self.lower:
        self.currentRatio = self.tickCD()
      else:
        self.reduce = False
        
    else: 
      if self.currentRatio < self.lower:  # ratio too low, increase
        poss = self.tickAir()
        if poss < self.target:
          self.currentRatio = poss
        else:
          self.currentRatio = self.tickNone()
      elif self.currentRatio < self.upper:  #ratio too low. Increase if burst would not be too large
        self.currentRatio = self.tickNone()
      else:
        self.reduce = True
        self.tickCD()
    return round(self.currentRatio*100, 3)



    


