from time import sleep
import math

class SystemTemp():
	def __init__(self, target):
		self.V 				= 29
		self.Ta 			= 296.0     # J/sec from heater
		self.T0 			= 296.0     # value at start of cooling phase
		self.Q  			= .5      	# J/sec from heater
		self.R  			= 0.08257 	# ideal gas constant
		self.C  			= 1.005   	# heat capacity of air, constant pressure
		self.P  			= 1.00263 	# pressure in CO, appx
		self.T  			= 296.0     # Current Temp    
		self.k 				= .05 			# cooling constant
		self.heat_on	= False			# heater is on
		self.t 				= 0					# time since cooling started (secs)
		self.lower 		= target + 272.15
		self.upper		= target + 274.15

	def printParams(self):
		print("Range: {} - {}\n".format(self.lower, self.upper))

	def printState(self):
		print("t: {} Ta: {} T0: {} T: {}".format(self.t, self.Ta, self.T0, self.T))

	def heatTick(self):
		n = (self.P * self.V)/(self.R * self.T)
		self.T = round( self.T + (self.Q)/(self.C * n), 3)

	def idleTick(self):
		self.T = round(self.Ta + (self.T0 - self.Ta)*math.exp(-self.k*self.t), 3)
		self.t = self.t + 1

	def tickMaintain(self):
		if self.heat_on:
			if self.T < self.upper:								# if lower than upper threshold, continue heating
				self.heatTick()	
			else:																	# otherwise, toggle heat off and initialize idle cycle
				self.T0 = self.T
				self.heat_on = False
				self.idleTick()

		else:
			if self.T > self.lower:								# if higher than lower threshold, continue idling
				self.idleTick()
			else:	
				self.t = 0													# otherwise, turn heater on and initialize heating cycle
				self.heat_on = True
				self.heatTick()
		return self.T

