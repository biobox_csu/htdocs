# Delete existing schema
DROP SCHEMA biobox;
CREATE DATABASE biobox;

# Generates control log and trials tables for interface
# and environmental control
USE biobox;
CREATE TABLE log(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	pres FLOAT DEFAULT 0,
	temp FLOAT DEFAULT 0,
	ox1 FLOAT DEFAULT 0,
	ox2 FLOAT DEFAULT 0,
	hum FLOAT DEFAULT 0
);

#create default row
INSERT INTO log () VALUES ();

CREATE TABLE control(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255),
	target FLOAT NOT NULL DEFAULT 0,
	active BOOL NOT NULL DEFAULT 0
);

INSERT INTO control (name) VALUES ('O2');
INSERT INTO control (name) VALUES ('temp');
INSERT INTO control (name) VALUES ('focus');
INSERT INTO control (name, target) VALUES ('log_intergal', 2);
INSERT INTO control (name, active) VALUES ('xRate',1);
INSERT INTO control (name, active) VALUES ('yRate',1);
INSERT INTO control (name, active) VALUES ('zRate',1);


CREATE TABLE trials(
	trial INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	experiment VARCHAR(255),
	time_start DATETIME,
	time_stop DATETIME,
	data_dir VARCHAR(255)
);
